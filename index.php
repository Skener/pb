<?php

namespace pb;

require __DIR__ . '/vendor/autoload.php';

use pb\App\Post;
use pb\Core\Router;
use pb\App\Aes;


session_start ();


$router = new Router();

$url = $_SERVER['QUERY_STRING'];
$router->add ( 'home', [ 'controller' => 'Home', 'action' => 'index' ] );
$router->add ( 'message/show', [ 'controller' => 'Message', 'action' => 'show' ] );

$router->add('{Message}/{show}/{id:\d+}');

//var_dump ($router->getRoutes ());


try {
	$router->dispatch ( $_SERVER['QUERY_STRING'] );
} catch ( \Exception $e ) {
	$e->getMessage ();
}




