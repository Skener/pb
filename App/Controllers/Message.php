<?php

namespace App\Controllers;


use pb\Core\Router;
use \pb\Core\View;
use \pb\App\Post;
use \pb\App\Aes;


class Message extends \pb\Core\Controller {

	public function show () {

		$id   = uniqid ( '1', false );
		$seId = $_SESSION['id'] = $id;
		if ( ( $_SERVER['REQUEST_METHOD'] === 'POST' ) && ( ! empty( $_POST['submit'] ) ) )  :
			$post      = new Post();
			$message   = $post->message;
			$key       = $_POST['secret'];
			$blockSize = 256;
			$aes       = new Aes( $message, $key, $blockSize );
			$enc       = $aes->encrypt ();
			$aes->setData ( $enc );
			$dec = $aes->decrypt ();

			View::renderTemplate ( 'Comments/index.html', [
				'comments' => $message,
				'id'       => $id,
				'enc'      => $enc,
				'dec'      => $dec,
				'seId'     => $seId
			] );

		endif;
	}

	public function id () {
		//$id = $_GET['id'];
		//echo $id;
		$router = new Router();
		var_dump ( $router->getParams () );
		//echo 'id';
	}


}
