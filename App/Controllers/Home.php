<?php

namespace App\Controllers;

use pb\Core\View;



class Home extends \pb\Core\Controller {


	public function indexAction () {
		View::renderTemplate ( 'Home/home.html' );
	}
}
