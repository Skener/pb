<?php

namespace pb\App;


class Post {
	public $message = '';
	public $errors = [];


	public function __construct () {
		if ( isset( $_POST['message'] ) ) {
			$this->validate ();
			$this->message = filter_var ( $_POST['message'], FILTER_SANITIZE_STRING );
		}
	}

	public function readMessage () {
		echo $this->message;

	}

	public function validate () {
		if ( $this->message == '' ) {
			$this->errors = 'Вибачте, це поле обовязкове';
			//return false;
			//$this->redirect('/home');
		}

	}

	public function redirect ( $url ) {
		header ( 'Location: http://' . $_SERVER['HTTP_HOST'] . $url, true, 303 );
		exit;
	}
}